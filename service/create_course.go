package service

import (
	"context"
	"database/sql"

	"gitee.com/WisdomClassroom/Course/glb"
	"gitee.com/WisdomClassroom/core"
	"gitee.com/WisdomClassroom/core/models"
	"gitee.com/WisdomClassroom/core/protobufs/v1/pb"
	"google.golang.org/grpc/metadata"
)

func (s *Service) CreateCourse(ctx context.Context, request *pb.CreateCourseRequest) (*pb.CreateCourseResponse, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		glb.Logger.Error("no metadata")
	}
	token, err := core.UnpackTokenFromMetadata(md)
	if err != nil {
		return &pb.CreateCourseResponse{Status: &pb.ResponseStatus{
			Code: core.ResponseStatusCodeNotAuth, Message: err.Error(),
		}}, nil
	}

	if token.Type != core.UserTypeTeacher {
		return &pb.CreateCourseResponse{Status: &pb.ResponseStatus{
			Code:    core.ResponseStatusCodeOtherError,
			Message: "用户没有权限",
		}}, nil
	}

	course := &models.Course{
		Name:        sql.NullString{String: request.GetName(), Valid: true},
		Description: sql.NullString{String: request.GetDescription(), Valid: true},
	}
	err = glb.DB.Create(course).Error
	if err != nil {
		if core.IsDuplicateKeyErr(err) {
			return &pb.CreateCourseResponse{Status: &pb.ResponseStatus{
				Code:    core.ResponseStatusCodeOtherError,
				Message: "此课程名称已存在",
			}}, nil
		}
		glb.Logger.Error(err.Error())
		return createCourseRespSvrErr, nil
	}

	return &pb.CreateCourseResponse{
		Status: &pb.ResponseStatus{Code: core.ResponseStatusCodeSuccess},
		UUID:   course.UUID,
	}, nil
}
