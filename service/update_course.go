package service

import (
	"context"
	"database/sql"

	"gitee.com/WisdomClassroom/Course/glb"
	"gitee.com/WisdomClassroom/core"
	"gitee.com/WisdomClassroom/core/models"
	"gitee.com/WisdomClassroom/core/protobufs/v1/pb"
	"google.golang.org/grpc/metadata"
	"gorm.io/gorm"
)

func (s *Service) UpdateCourse(ctx context.Context, request *pb.UpdateCourseRequest) (*pb.UpdateCourseResponse, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		glb.Logger.Error("no metadata")
	}
	token, err := core.UnpackTokenFromMetadata(md)
	if err != nil {
		return &pb.UpdateCourseResponse{Status: &pb.ResponseStatus{
			Code: core.ResponseStatusCodeNotAuth, Message: err.Error(),
		}}, nil
	}

	course := &models.Course{UUID: request.GetUUID()}

	err = glb.DB.Select("uuid").Take(course).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return &pb.UpdateCourseResponse{Status: &pb.ResponseStatus{
				Code:    core.ResponseStatusCodeOtherError,
				Message: "课程不存在",
			}}, nil
		}
		glb.Logger.Error(err.Error())
		return updateCourseRespSvrErr, nil
	}

	if !token.IsManager {
		managers := make([]models.User, 0, 1)
		err = glb.DB.Model(course).Association("Managers").Find(&managers, "manager_id = ?", token.AccountID)
		if err != nil {
			glb.Logger.Error(err.Error())
			return updateCourseRespSvrErr, nil
		}
		if len(managers) == 0 {
			return &pb.UpdateCourseResponse{Status: &pb.ResponseStatus{
				Code:    core.ResponseStatusCodeNotAuth,
				Message: "用户没有权限",
			}}, nil
		}
	}

	course.Name = sql.NullString{String: request.GetName(), Valid: true}
	course.Description = sql.NullString{String: request.GetDescription(), Valid: true}
	err = glb.DB.Model(course).Updates(course).Error
	if err != nil {
		glb.Logger.Error(err.Error())
		return updateCourseRespSvrErr, nil
	}

	return &pb.UpdateCourseResponse{
		Status: &pb.ResponseStatus{Code: core.ResponseStatusCodeSuccess},
	}, nil
}
