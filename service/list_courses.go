package service

import (
	"context"
	"gitee.com/WisdomClassroom/core"

	"gitee.com/WisdomClassroom/Course/glb"
	"gitee.com/WisdomClassroom/core/models"
	"gitee.com/WisdomClassroom/core/protobufs/v1/pb"
)

func (s Service) ListCourses(ctx context.Context, request *pb.Empty) (*pb.ListCoursesResponse, error) {
	courses := make([]models.Course, 0, 8)
	err := glb.DB.Find(&courses).Error
	if err != nil {
		glb.Logger.Error(err.Error())
		return listCourseRespSvrErr, nil
	}

	coursesResp := make([]*pb.ListCoursesResponse_CourseBasicInfo, 0, 8)
	for _, course := range courses {
		coursesResp = append(coursesResp, &pb.ListCoursesResponse_CourseBasicInfo{
			UUID:        course.UUID,
			Name:        course.Name.String,
			Description: course.Description.String,
		})
	}

	return &pb.ListCoursesResponse{
		Status:  &pb.ResponseStatus{Code: core.ResponseStatusCodeSuccess},
		Courses: coursesResp,
	}, nil
}
